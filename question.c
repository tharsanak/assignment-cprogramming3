#include<stdio.h>
#define PerformanceCost 500
#define PerAttendance 3

int NoOfAttendance(int Price);
int Income(int Price);
int Cost(int Price);
int Profit(int Price);

int NoOfAttendance(int Price){
    return 120-(Price-15)*20/5;
}
int Income(int Price){
    return NoOfAttendance(Price)*Price;
}
int Cost(int Price){
	return PerformanceCost+PerAttendance*NoOfAttendance(Price);
}
int Profit(int Price){
	return Income(Price)-Cost(Price);
}
int main()
{int price;
printf("Expected profit for ticket price\n\n");
for(price=5;price<=45;price+=5){
	printf("Ticket price=%d\t Profit=%d\n",price,Profit(price));
	printf("...............................................\n");
}
return 0;
}
